var StudentsList = React.createClass({
    getInitialState: function() {
        return {data: []};
      },
      
    componentDidMount: function() {
        $.ajax({
          url: "/assets/json/students.json",
          dataType: 'json',
          cache: false,
          success: function(data) {
            this.setState({data: data});
          }.bind(this),
          error: function(xhr, status, err) {
            console.error(this.props.url, status, err.toString());
          }.bind(this)
        });
      },
    
    render: function() {
        var studentNodes = this.state.data.map(function(student) {
            return (
                    <CompleteStudent key={student.id} name={student.name}
                    number={student.number} photo={student.photo}/>
            );
        });
    return (
      <div>
        {studentNodes}
      </div>
    );
  }
});