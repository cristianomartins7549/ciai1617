package ciai;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Student;

@Controller
@RequestMapping(value = "/students")
public class StudentController {

	Student[] students = { new Student(0, "Ingrid Daubechies", 19), new Student(1, "Jacqueline K. Barton", 18),
			new Student(2, "Jane Goodall", 20), new Student(3, "Jocelyn Bell Burnell", 21),
			new Student(4, "Johannes Kepler", 22), new Student(5, "Lene Vestergaard Hau", 17) };

	@RequestMapping(value = "")
	public @ResponseBody List<Student> getStudents() {
		return Arrays.asList(students);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Student getStudent(@PathVariable int id) {
		return students[id];
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String getViewStudent(@PathVariable int id) {
		return "student";
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String getViewAllStudents() {
		return "studentOrig";
	}
}
